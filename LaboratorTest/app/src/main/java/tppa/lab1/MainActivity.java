package tppa.lab1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity  implements ExampleDialog.ExampleDialogListener{

    private ListView listview;
    private TextView textView;
    private Button button;
    private Button buttonS;
    private Button buttonL;
    private Button buttonC;
    private final ArrayList<String> productList = new ArrayList<>();
    private final ArrayList<String> descriptionList = new ArrayList<>();
    private ArrayList<String> temporaryFilteredProducts = new ArrayList<>();

    private static final String FILE_NAME = "example.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView)findViewById(R.id.listView);
        textView = (TextView)findViewById(R.id.textView);
        button = (Button)findViewById(R.id.Button);
        buttonS = (Button)findViewById(R.id.ButtonS);
        buttonL = (Button)findViewById(R.id.ButtonL);
        buttonC = (Button)findViewById(R.id.ButtonC);

        productList.add("Produs1");
        productList.add("Produs2");
        productList.add("Produs3");
        productList.add("Produs4");
        productList.add("Produs5");

        descriptionList.add("Descriere1");
        descriptionList.add("Descriere2");
        descriptionList.add("Descriere3");
        descriptionList.add("Descriere4");
        descriptionList.add("Descriere5");

        if (savedInstanceState != null) {
            CharSequence savedText = savedInstanceState.getCharSequence("Description");
            textView.setText(savedText);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               sendMessage();
            }
        });

        buttonS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sensors();
            }
        });

        buttonL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location();
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera();
            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                textView.setText(descriptionList.get(position));
        }});

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, productList);
        listview.setAdapter(arrayAdapter);
    }

    @Override
    protected void onStart()
    {
        FileInputStream fis = null;
        super.onStart();
        try {
            fis = openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }

            textView.setText(sb.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Toast.makeText(getApplicationContext(),"onStart called", Toast.LENGTH_LONG).show();
        Log.d("MainActivity", "onPause called");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Toast.makeText(getApplicationContext(), "onPause called", Toast.LENGTH_LONG).show();
        SaveFileToInternalStorage();
        Log.d("MainActivity", "onPause called");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Toast.makeText(getApplicationContext(), "onResume called", Toast.LENGTH_SHORT).show();
        Log.d("MainActivity", "onResume called");
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstance)
    {
        super.onSaveInstanceState(savedInstance);
        Toast.makeText(getApplicationContext(), "onSaveInstanceState called", Toast.LENGTH_SHORT).show();
        Log.d("MainActivity", "onSaveInstanceState called");
        savedInstance.putCharSequence("Description", textView.getText());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstance)
    {
        super.onRestoreInstanceState(savedInstance);
        Toast.makeText(getApplicationContext(), "onRestoreInstanceState called", Toast.LENGTH_SHORT).show();
        Log.d("MainActivity", "onRestoreInstanceState called");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.products:
                Log.d("OptionsMenu", "First item pressed");
                Toast.makeText(getApplicationContext(), "optionsMenu First option called", Toast.LENGTH_SHORT).show();
                break;
            case R.id.services:
                Log.d("OptionsMenu", "Second item pressed");
                Toast.makeText(getApplicationContext(), "optionsMenu Second option called", Toast.LENGTH_SHORT).show();
                break;
            case R.id.filter:
                Log.d("OptionsMenu", "Third item pressed");
                Toast.makeText(getApplicationContext(), "optionsMenu third option called", Toast.LENGTH_SHORT).show();
                OpenDialog();
                break;
            case R.id.preferences:
                Log.d("OptionsMenu", "Fourth item pressed");
                Toast.makeText(getApplicationContext(), "optionsMenu fourth option called", Toast.LENGTH_SHORT).show();
                ShowPreferences();
                break;
        }
        return true;
    }

    private void OpenDialog() {
        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getSupportFragmentManager(), "Filter");
    }

    public void sensors() {
        Intent intent = new Intent(this, ThirdActivity.class);
        startActivityForResult(intent, 1);
    }
    public void location() {
        Intent intent = new Intent(this, LocationActivity.class);
        startActivityForResult(intent, 1);
    }

    public void camera() {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivityForResult(intent, 1);
    }

    public void sendMessage() {
        Intent intent = new Intent(this, SecondaryActivity.class);
        String message = textView.getText().toString();
        intent.putExtra("transferText", String.valueOf(message));
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if((resultCode == RESULT_OK) && (requestCode == 1)) {
            if (intent.hasExtra("toReturn")) {
                Log.d("toReturn", "The request from the other activity was successful");
            }
        }
    }

    @Override
    public void filterResults(String filter) {
        for (int i=0; i<this.productList.size(); i++) {
            if (this.productList.get(i).toLowerCase().contains(filter.trim())) {
                temporaryFilteredProducts.add(this.productList.get(i));
            }
        }
        refreshListView();
    }

    protected void ShowPreferences() {
        startActivity(new Intent(this,MyPrefActivity.class));
    }

    private void refreshListView() {
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, temporaryFilteredProducts);
        listview.setAdapter(arrayAdapter);
    }

    protected void SaveFileToInternalStorage() {
        FileOutputStream fos;
        try {
            fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(textView.getText().toString().getBytes());
//            Toast.makeText(this, "Saved to " + getFilesDir() + "/" + FILE_NAME,
//                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
