package tppa.lab1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import tppa.lab1.R;

public class SecondaryActivity extends Activity {

    private TextView textView;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        Intent intent = getIntent();
        String message = intent.getStringExtra("transferText").toString();

        textView = findViewById(R.id.textView2);
        button = (Button) findViewById(R.id.Button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
        textView.setText(message);
    }

    public void finish() {
        Intent data = new Intent();
        if(textView.getText() != null){
            data.putExtra("transferText", textView.getText());
            setResult(RESULT_OK, data);
        }
        else {
            data.putExtra("transferText", "Bad request");
            setResult(RESULT_CANCELED, data);
        }
        super.finish();
    }

    public void sendMessage() {
        Uri uri = Uri.parse("smsto:+40741569034");
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "text to send");
        startActivity(it);
    }
}
