package tppa.lab1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Button;
import android.widget.CheckBox;

public class MyPrefActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("my_prefs");
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        SavePrefs();
    }


    protected void SavePrefs() {
        SharedPreferences pref = getSharedPreferences("my_prefs", 0);
        SharedPreferences.Editor edit = pref.edit();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        edit.putInt("downloadType", sp.getInt("downloadType", -1));
        edit.putBoolean("applicationUpdates", sp.getBoolean("applicationUpdates", false));
        edit.commit();
    }

}