package tppa.lab1;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class ThirdActivity extends Activity implements SensorEventListener {
    private Sensor sensorAccelerometer;
    private SensorManager sensorManager;
    private ListView listView;
    private TextView textViewValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        listView = (ListView)findViewById(R.id.listView);
        textViewValue = (TextView)findViewById(R.id.textView);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor s: sensorList)
        {
            Log.v("TAG",String.format("Name:%s, Vendor:%s,Type:%d",s.getName(),s.getVendor(),s.getType()));
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, sensorList);
        listView.setAdapter(arrayAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sensorManager != null)
            sensorManager.registerListener(this, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (sensorManager != null)
            sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor s, int value) {
        Log.v("TAG", "Accuracy Changed");
        Log.v("TAG", "Sensor = " + s.getName());
        Log.v("TAG", "Vendor = " + s.getVendor());
        Log.v("TAG", "Version = " + String.valueOf(s.getVersion()));
        Log.v("TAG", "Value = " + String.valueOf(value));
    }


    public void onSensorChanged(SensorEvent s) {
        Log.v("TAG", "Sensor Changed");
        Log.v("TAG", "Sensor = " + s.sensor.getName());
        String values = "";
        for (int tr = 0; tr < s.values.length; tr++)
            values += String.format("%f,", s.values[tr]);
        Log.v("TAG", "Values = " + values);
        textViewValue.setText(values);
    }
}
